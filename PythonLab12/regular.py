import re

def searchInFile():
    count = 0
    file = open('igm.txt')
    for line in file:
        if re.search(
        '''((02:19:[3-5][0-9])|(02:[2-5][0-9]:[0-5][0-9])
        |(0[3-9]:[0-5][0-9]:[0-5][0-9])|(10:[0-5][0-9]:[0-5][0-9])
        |(11:0[0-9]:[0-2][0-9]))'''
        '.*GET.*[200-399]',
        str(line)):
            count+=1
    print(count)
    file.close()


if __name__ == "__main__":
    searchInFile()
